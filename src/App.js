import React, { Component } from 'react';
import AddForm from './components/AddForm';
import WorksForm from './components/WorksForm';
import Control from './components/Control'
import { connect } from 'react-redux'
import * as actions from './actions/index'

class App extends Component {
  render() {
    const { isDisplayForm } = this.props
    return (
      <div className="container">
        <hr />
        <div className="text-center">
          <h1>WORKS MANAGE</h1>
        </div>
        <hr />
        <div className="row">
          <div className={ isDisplayForm ? 'col-xs-4 col-sm-4 col-md-4 col-lg-4' : '' }>
            {/* Form */}
            {
              isDisplayForm
              ?
              <AddForm />
              :
              ''
            }
          </div>
          <div className={ isDisplayForm ? 'col-xs-8 col-sm-8 col-md-8 col-lg-8' : 'col-xs-12 col-sm-12 col-md-12 col-lg-12' }>
            <button className="btn btn-success"
              onClick={ () => this.props.toggleForm() }
            >
              <i className="far fa-plus-square"></i> &nbsp; Add New Task
            </button>
            <div className="row" style={{ marginLeft: 0 }}>
              <Control 
                onSort={ (objSort) => this.setState({ sort: objSort }) }
              />
            </div>
            <div className="row" style={{ marginTop: 15 }}>
              <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <WorksForm 
                  editWork={ this.editWork }
                  filter={ this.onFilter }
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    isDisplayForm: state.isDisplayForm
  }
}

const mapDispatchToProps = dispatch => {
  return {
    toggleForm: () => {
      dispatch(actions.toggleForm())
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);