import React from 'react';
import { connect } from 'react-redux'
import * as actions from '../actions/index'

const Work = (props) => {
  const { work, index } = props
  return (
    <tr className='text-center'>
      <th scope="row" style={{ textAlign: 'center' }}>{index + 1}</th>
      <td>{work.name}</td>
      <td>
        {
          work.status === 'Pending'
            ?
            <span className="label label-danger" onClick={ () => props.changeWorkStatus(work, 'Doing') }>{work.status}</span>
            :
            (
              work.status === 'Doing'
                ?
                <span className="label label-warning"onClick={ () => props.changeWorkStatus(work, 'Done') }>{work.status}</span>
                :
                <span className="label label-success" onClick={ () => props.changeWorkStatus(work, 'Pending') }>{work.status}</span>
            )
        }
      </td>
      <td>
        <button type="button" className="btn btn-success" onClick={ () => {
          props.editWork(work)
          props.openForm()
        }}> <i className="fas fa-pencil-alt"></i> Edit</button>
        &nbsp; &nbsp;
            <button type="button" className="btn btn-danger" onClick={ () => props.deleteWork(work) }><i className="far fa-trash-alt"></i> Delete</button>
      </td>
    </tr>
  );
};

const mapDispatchToProps = dispatch => {
  return {
    changeWorkStatus: (item, status) => {
      dispatch(actions.changeWorkStatus(item, status))
    },
    deleteWork: (work) => {
      dispatch(actions.deleteWork(work))
    },
    openForm: () => {
      dispatch(actions.openForm())
    },
    editWork: (work) => {
      dispatch(actions.editWork(work))
    }
  }
}

export default connect(
  null,
  mapDispatchToProps
)(Work)