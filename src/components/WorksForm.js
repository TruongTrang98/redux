import Work from './Work';
import React, { useState } from 'react';
import { connect } from 'react-redux'
import * as actions from '../actions/index'

const WorksForm = (props) => {
  const [filterName, changeFilterName] = useState('')
  const [filterStatus, changeFilterStatus] = useState('All')

  let { works, filterTable, searchKeyword, sort } = props
  if (filterTable) {
    if (filterTable.filterName) {
      works = works.filter(item => item.name.toLowerCase().indexOf(filterTable.filterName.toLowerCase()) !== -1)
    }
    if (filterTable.filterStatus) {
      works = works.filter(item => filterTable.filterStatus === 'All' ? item : item.status === filterTable.filterStatus)
    }
  }
  if (searchKeyword) {
    works = works.filter(item => item.name.toLowerCase().indexOf(searchKeyword.toLowerCase()) !== -1)
  }
  if (sort.by !== '') {
    if (sort.by === 'name') {
      works.sort((a, b) => {
        if (a.name > b.name) return sort.value
        else if (a.name < b.name) return -sort.value
        else return 0
      })
    } else {
      works = works.filter(item => item.status === sort.value)
    }
  }
  return (
    <table className="table table-bordered table-hover">
      <thead>
        <tr>
          <th className='text-center'>#</th>
          <th className='text-center'>Name</th>
          <th className='text-center'>Status</th>
          <th className='text-center'></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td></td>
          <td>
            <input type='text' className='form-control' name='filterName' value={filterName} onChange={e => {
              changeFilterName(e.target.value)
              props.filterInTable(e.target.value, filterStatus)
            }} />
          </td>
          <td>
            <select className='form-control' name='filterStatus' value={filterStatus} onChange={e => {
              changeFilterStatus(e.target.value)
              props.filterInTable(filterName, e.target.value)
            }}>
              <option value='All'>All</option>
              <option value='Pending'>Pending</option>
              <option value='Doing'>Doing</option>
              <option value='Done'>Done</option>
            </select>
          </td>
          <td></td>
        </tr>
        {
          works.map((item, index) => (
            <Work
              key={index}
              work={item}
              index={index}
              editWork={() => props.editWork(item)}
              deleteWork={() => props.deleteWork(item)}
            />
          ))
        }
      </tbody>
    </table>
  );
};

const mapStateToProps = state => {
  return {
    works: state.works,
    filterTable: state.filterInTable,
    searchKeyword: state.search,
    sort: state.filterMore
  }
}

const mapDispatchToProps = dispatch => {
  return {
    filterInTable: (filterName, filterStatus) => {
      dispatch(actions.filterInTable({ filterName, filterStatus }))
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(WorksForm)