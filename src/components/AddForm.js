import React, { Component } from 'react';
import { connect } from 'react-redux'
import * as actions from '../actions/index'

class AddForm extends Component {
  constructor(props) {
    super(props)
    this.state = {
      id: '',
      name: '',
      status: 'Pending'
    }
  }

  onChange = (e) => {
    var target = e.target
    var name = target.name
    var value = target.value
    this.setState({
      [name]: value
    })
  }

  randomStr() {
    return Math.floor((1 + Math.random()) * 0x1000).toString(16).substring(1)
  }

  generateID() {
    return this.randomStr() + this.randomStr() + '-' + this.randomStr() + '-' + this.randomStr()
  }

  onSubmit = (e) => {
    const { name, status } = this.state
    if (name === '') {
      alert('You must fill work\'s name')
      e.preventDefault()
    }
    else {
      e.preventDefault()            
      if (this.state.id !== '') {
        const { id, name, status } = this.state
        this.props.saveEditWork({ id, name, status })
        this.props.editWork()
        this.props.closeForm()
      } else {
        this.props.addNewWork({ id: this.generateID(), name, status })
      }
      this.resetValue(e)
    }
  }

  resetValue = (e) => {
    e.preventDefault()
    this.setState({
      id: '',
      name: '',
      status: 'Pending'
    })
  }

  componentWillMount() {
    if (this.props.updateValue) {
      this.setState({
        id: this.props.updateValue.id,
        name: this.props.updateValue.name,
        status: this.props.updateValue.status
      })
    }
  }

  render() {
    return (
      <div className="panel panel-warning">
        <div className="panel-heading">
          <h3 className="panel-title">
            {
              !!this.props.updateValue && this.props.updateValue.id !== '' ? 'Edit Work' :'Add New Work'
            }
            <span className="fa fa-times-circle" style={{ float: 'right' }} onClick={() => {
              this.props.editWork()
              this.props.closeForm()
            }}></span>
          </h3>
        </div>
        <div className="panel-body">
          <form onSubmit={this.onSubmit}>
            <div className="form-group">
              <label>Name:</label>
              <input
                type="text"
                className="form-control"
                id=""
                placeholder="Work's Name"
                name='name'
                value={this.state.name}
                onChange={this.onChange}
              />
              <label style={{ marginTop: 10 }}>Status:</label>
              <select className="form-control form-control-lg" name='status' value={this.state.status} onChange={this.onChange}>
                <option>Pending</option>
                <option>Doing</option>
                <option>Done</option>
              </select>
            </div>
            <div className='text-center'>
              <button className="btn btn-success"> <i className="fas fa-plus"></i> { !!this.props.updateValue && this.props.updateValue.id === '' ? 'Save' : 'Add'}</button> &nbsp;
              <button className="btn btn-danger" onClick={this.resetValue}><i className="fas fa-trash-restore"></i> Reset</button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    updateValue: state.editWork
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addNewWork: (work) => (
      dispatch(actions.addNewWork(work))
    ),
    closeForm: () => {
      dispatch(actions.closeForm())
    },
    editWork: () => {
      dispatch(actions.editWork(null))
    },
    saveEditWork: (work) => {
      dispatch(actions.saveEditWork(work))
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddForm);