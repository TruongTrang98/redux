import * as actionTypes from '../constains/ActionTypes'

const initialState = false  

const changeFormStatus = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.TOGGLE_FORM: {
      return !state
    }
    case actionTypes.OPEN_FORM: {
      return true
    }
    case actionTypes.CLOSE_FORM: {
      return false
    }
    default: return state
  }
}

export default changeFormStatus