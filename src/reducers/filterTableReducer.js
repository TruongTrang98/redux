import * as actionTypes from '../constains/ActionTypes'

const initialState = {
  name: '',
  status: 'All'
}

const filterTableReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FILTER_IN_TABLE: {
      return action.value;
    }
    default: return state;
  }
}

export default filterTableReducer