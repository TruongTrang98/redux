import * as actionTypes from '../constains/ActionTypes'

const initialState = {
  by: 'name',
  value: 1
}

const filterMoreReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FILTER_MORE: {
      return {
        by: action.filter.by,
        value: action.filter.value
      }
    }
    default: return state
  }
}

export default filterMoreReducer