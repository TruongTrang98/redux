import * as actionTypes from '../constains/ActionTypes'

const initialState = null

const editWorkReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.EDIT_WORK: {
      return action.value;
    }
    default: return state;
  }
}

export default editWorkReducer