import * as actionTypes from '../constains/ActionTypes'

const initialState = (localStorage && localStorage.getItem('works')) ? JSON.parse(localStorage.getItem('works')) : []

const worksReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.WORKS: {
      return state
    }
    case actionTypes.ADD_NEW_WORK: {
      const newState = [...state, action.work]
      localStorage.setItem('works', JSON.stringify(newState))
      return newState
    }
    case actionTypes.CHANGE_WORK_STATUS: {
      const newState = [...state]
      newState.forEach(item => {
        if (item === action.payload.work) {
          item.status = action.payload.newStatus
        }
      })
      localStorage.setItem('works', JSON.stringify(newState))
      return newState
    }
    case actionTypes.DELETE_WORK: {
      const newState = [...state]
      newState.forEach((item, index) => {
        if (item === action.work) {
          newState.splice(index, 1)
        }
      })
      localStorage.setItem('works', JSON.stringify(newState))
      return newState
    }
    case actionTypes.SAVE_EDIT_WORK: {
      state.forEach(item => {
        if (item.id === action.work.id) {
          item.id = action.work.id
          item.name = action.work.name
          item.status = action.work.status
        }
      })
      localStorage.setItem('works', JSON.stringify(state))
      return [...state]
    }
    default: {
      return state;
    }
  }
}

export default worksReducer