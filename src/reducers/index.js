import { combineReducers } from 'redux'
import works from './worksReducer'
import isDisplayForm from './formStatusReducer'
import editWork from './editWorkReducer'
import filterInTable from './filterTableReducer'
import search from './searchReducer';
import filterMore from './filterMoreReducer'

const rootReducer = combineReducers({
  works,
  isDisplayForm,
  editWork,
  filterInTable,
  search,
  filterMore
})

export default rootReducer