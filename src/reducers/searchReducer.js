import * as actionTypes from '../constains/ActionTypes'

const initialState = ''

const searchReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SEARCH_BY_KEYWORD: {
      return action.keyword
    }
    default: return state
  }
}

export default searchReducer