import * as actionTypes from '../constains/ActionTypes'

export const works = () => {
  return {
    type: actionTypes.WORKS
  }
}

export const addNewWork = (work) => {
  return {
    type: actionTypes.ADD_NEW_WORK,
    work
  }
}

export const changeWorkStatus = (work, newStatus) => {
  return {
    type: actionTypes.CHANGE_WORK_STATUS,
    payload: {
      work, newStatus
    }
  }
}

export const deleteWork = (work) => {
  return {
    type: actionTypes.DELETE_WORK,
    work
  }
}

export const editWork = (value) => {
  return {
    type: actionTypes.EDIT_WORK,
    value
  }
}

export const saveEditWork = (work) => {
  return {
    type: actionTypes.SAVE_EDIT_WORK,
    work
  }
}

export const toggleForm = () => {
  return {
    type: actionTypes.TOGGLE_FORM
  }
}

export const openForm = () => {
  return {
    type: actionTypes.OPEN_FORM
  }
}

export const closeForm = () => {
  return {
    type: actionTypes.CLOSE_FORM
  }
}

export const searchByKeyword = (keyword) => {
  return {
    type: actionTypes.SEARCH_BY_KEYWORD,
    keyword
  }
}

export const filterInTable = (value) => {
  return {
    type: actionTypes.FILTER_IN_TABLE,
    value
  }
}

export const filterMore = (filter) => {
  return {
    type: actionTypes.FILTER_MORE,
    filter
  }
}